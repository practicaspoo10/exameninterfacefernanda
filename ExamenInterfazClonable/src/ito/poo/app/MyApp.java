package ito.poo.app;

import clases.ito.poo.Persona;

public class MyApp {
	
	static void run() throws CloneNotSupportedException {
		
		Persona pers1= new Persona("Elena Castellaņo", 264816478);
		Persona pers2= pers1;
		
		System.out.println(pers1);
		
		System.out.println("Persona Clonada");
		Persona pers3= (Persona) pers1.clone();
		
		System.out.println("Se esta modificando el nombre de la persona 2");
		pers2.setNombre("Marely Arellano");
		
		System.out.println ("Persona1: " + pers1 + "\nPersona2: " + pers2 + "\nPersona3: " + pers3);
		
	}

	public static void main(String[] args) throws CloneNotSupportedException{
		// TODO Auto-generated method stub
		run();

	}

}
